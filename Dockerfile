FROM ruby:2.5
MAINTAINER Harish Ramachandran <harish@gitlab.com>

ENV APP_HOME /hello-sinatra
RUN mkdir $APP_HOME
WORKDIR $APP_HOME
COPY . $APP_HOME
RUN bundle install

# Start server
ENV PORT 5000
EXPOSE 5000
CMD ["ruby", "main.rb"]

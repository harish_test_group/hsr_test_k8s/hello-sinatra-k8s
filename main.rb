require 'sinatra' 

set :bind, '0.0.0.0'
set :port, 5000

get '/' do 
  "Welcome to a Kubernetes Demo"
end

get '/hello' do
  "Hello, Support Team!"
end

get '/goodbye' do
  "Goodbye, Support Team!"
end
